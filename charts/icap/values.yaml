# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
#
# SPDX-License-Identifier: Apache-2.0
---
# The global properties are used to configure multiple charts at once.
global:
  # -- Container registry address.
  registry: "docker.io"

  # -- Credentials to fetch images from private registry
  # Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  #
  # imagePullSecrets:
  #   - "docker-registry"
  #
  imagePullSecrets: []

  # This claim is used to share the database with other ClamAV services.
  persistence:
    # -- Name of existing shared Persistent Volume Claim.
    tmpClaimName: "clamav-tmp"

# -- Affinity for pod assignment
# Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
# Note: podAffinityPreset, podAntiAffinityPreset, and nodeAffinityPreset will be ignored when it's set
affinity: {}

# -- Additional custom annotations to add to all deployed objects.
commonAnnotations: {}

# -- Additional custom labels to add to all deployed objects.
commonLabels: {}

# Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
containerSecurityContext:
  # -- Enable container privileged escalation.
  allowPrivilegeEscalation: false

  # -- Security capabilities for container.
  capabilities:
    drop:
      - "ALL"

  # -- Enable security context.
  enabled: true

  # -- Process user id.
  runAsUser: 100

  # -- Process group id.
  runAsGroup: 101

  # Set Seccomp profile.
  seccompProfile:
    # -- Disallow custom Seccomp profile by setting it to RuntimeDefault.
    type: "RuntimeDefault"

  # -- Mounts the container's root filesystem as read-only.
  readOnlyRootFilesystem: true

  # -- Run container as user.
  runAsNonRoot: true

# -- Array with extra environment variables to add to containers.
#
# extraEnvVars:
#   - name: FOO
#     value: "bar"
#
extraEnvVars: []

# -- Optionally specify extra list of additional volumes
extraVolumes: []

# -- Optionally specify extra list of additional volumeMounts.
extraVolumeMounts: []

# -- Provide a name to substitute for the full names of resources.
fullnameOverride: ""

# Container image section.
image:
  # -- Container registry address. This setting has higher precedence than global.registry.
  registry: "registry.souvap-univention.de"

  # -- Container repository string.
  repository: "souvap/tooling/images/c-icap"

  # -- Define an ImagePullPolicy.
  #
  # Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy
  #
  # "IfNotPresent" => The image is pulled only if it is not already present locally.
  # "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to
  #             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached
  #             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved
  #             digest, and uses that image to launch the container.
  # "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the
  #            kubelet attempts to start the container; otherwise, startup fails
  #
  imagePullPolicy: "IfNotPresent"

  # -- Define image tag.
  tag: "0.5.10@sha256:ca4574d66aa035621336329bf08b8de9f65dc0f6744e3950ca6a0c5ba8cbd8ba"

# -- Credentials to fetch images from private registry
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
#
# imagePullSecrets:
#   - "docker-registry"
#
imagePullSecrets: []

# -- Lifecycle to automate configuration before or after startup
lifecycleHooks: {}

# -- String to partially override release name.
nameOverride: ""

# -- Node labels for pod assignment
# Ref: https://kubernetes.io/docs/user-guide/node-selection/
nodeSelector: {}

#  Configure extra options for containers probes.
livenessProbe:
  # -- Enables kubernetes LivenessProbe.
  enabled: true
  # -- Number of failed executions until container is terminated.
  failureThreshold: 10
  # -- Delay after container start until LivenessProbe is executed.
  initialDelaySeconds: 15
  # -- Time between probe executions.
  periodSeconds: 20
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 5

# -- Pod Annotations.
# Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
podAnnotations: {}

# -- Pod Labels.
# Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/
podLabels: {}

# Pod Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
podSecurityContext:
  # -- Enable security context.
  enabled: true

  # -- If specified, all processes of the container are also part of the supplementary group
  fsGroup: 101

  # -- Change ownership and permission of the volume before being exposed inside a Pod.
  fsGroupChangePolicy: "Always"

#  Configure extra options for containers probes.
readinessProbe:
  # -- Enables kubernetes ReadinessProbe.
  enabled: true
  # -- Delay after container start until ReadinessProbe is executed.
  initialDelaySeconds: 15
  # -- Number of failed executions until container is terminated.
  failureThreshold: 10
  # -- Time between probe executions.
  periodSeconds: 20
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 5

# -- Set the amount of replicas of deployment.
replicaCount: 1

# Configure resource requests and limits.
#
# http://kubernetes.io/docs/user-guide/compute-resources/
#
resources:
  limits:
    # -- The max amount of CPUs to consume.
    cpu: 1
    # -- The max amount of RAM to consume.
    memory: "128Mi"
  requests:
    # -- The amount of CPUs which has to be available on the scheduled node.
    cpu: "10m"
    # -- The amount of RAM which has to be available on the scheduled node.
    memory: "16Mi"

# Define and create Kubernetes Service.
#
# Ref.: https://kubernetes.io/docs/concepts/services-networking/service
service:
  # -- Additional custom annotations
  annotations: {}

  # -- Enable kubernetes service creation.
  enabled: true

  # "ClusterIP" => Exposes the Service on a cluster-internal IP. Choosing this value makes the Service only reachable
  #                from within the cluster. This is the default that is used if you don't explicitly specify a type for
  #                a Service.
  # "NodePort" => Exposes the Service on each Node's IP at a static port (the NodePort). To make the node port
  #               available, Kubernetes sets up a cluster IP address, the same as if you had requested a Service of
  #               type: ClusterIP.
  # "LoadBalancer" => Exposes the Service externally using a cloud provider's load balancer.
  #
  # Ref.: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types

  # -- Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer".
  type: "ClusterIP"

  # Define the ports of Service.
  # You can set the port value to an arbitrary value, it will map the container port by name.
  #
  ports:
    icap:
      # -- Accessible port for ClamAV icap.
      port: 1344
      # -- Internal port for ClamAV icap.
      containerPort: 1344
      # -- Milter service protocol.
      protocol: "TCP"

# Service account to use.
# Ref.: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
serviceAccount:
  # -- Additional custom annotations for the ServiceAccount.
  annotations: {}

  # -- Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this
  # serviceAccount do not need to use K8s API.
  automountServiceAccountToken: false

  # -- Enable creation of ServiceAccount for pod.
  create: true

  # -- Additional custom labels for the ServiceAccount.
  labels: {}

settings:
  # -- The time in seconds after which a connection without activity can be canceled.
  timeout: 300

  # -- The maximum number of requests can be served by one connection.
  maxKeepAliveRequests: 100

  # -- The maximum time in seconds waiting for a new requests before a connection will be closed.
  keepAliveTimeout: 600

  # -- The initial number of server processes. Each server process generates a number of threads, which serve the
  # requests.
  startServers: 1

  # -- The maximum allowed number of server processes.
  maxServers: 4

  # -- If the number of the available threads is less than number, the c-icap server starts a new child.
  minSpareThreads: 10

  # -- If the number of the available threads is more than number, then the c-icap server kills a child.
  maxSpareThreads: 20

  # -- The number of threads per child process.
  threadsPerChild: 10

  # -- The maximum number of requests that a child process can serve.
  # After this number has been reached, the process dies.
  # The goal of this parameter is to minimize the risk of memory leaks and increase the stability of c-icap.
  # It can be disabled by setting its value to 0.
  maxRequestsPerChild: 0

  # -- The Administrator of this server. Used when displaying information about this server (logs, info service, etc).
  serverAdmin: "c-icap-admin"

  # -- A name for this server. Used when displaying information about this server (logs, info service, etc).
  serverName: "c-icap"

  # -- Optional path to the global temporary directory.
  tmpDir: "/var/tmp"

  # -- The maximum memory size in bytes taken by an object which is processed by c-icap . If the size of an object's
  # body is larger than the maximum size a temporary file is used.
  maxMemObject: 131072

  # -- The level of debugging information to be logged. The acceptable range of levels is between 0 and 10.
  debugLevel: 1

  # -- Enable or disable ICAP requests pipelining
  pipelining: "on"

  # -- Try to handle requests from buggy clients, for example ICAP requests missing "\r\n" sequences
  supportBuggyClients: "off"

  # -- Set it to on if you want to use username provided by the proxy server.
  # This is the recommended way to use users in c-icap. If the RemoteProxyUsers is off and c-icap configured to use
  # users or groups the internal authentication mechanism will be used.
  remoteProxyUsers: "off"

  # -- Additional custom configuration:
  additionalConfiguration: {}

  # -- The list of file types or groups of file types which will be scanned for viruses.
  # For supported types look in c-icap.magic configuration file.
  virusScanScanFileTypes: "TEXT DATA EXECUTABLE ARCHIVE GIF JPEG MSOFFICE"

  # -- The percentage of data that can be sent by the c-icap server before receiving the complete body of a request.
  # This feature in conjuction with the folowing can be usefull becouse if the download of the object takes a lot of
  # time the connection of web client to proxy can be expired.
  # It must be noticed that the data which delivered to the web client maybe contains a virus or a part of a virus and
  # can be dangerous. In the other hand partial data (for example 5% data of a zip or an exe file) in most cases can not
  # be used.
  # Set it to 0 to disable this feature.
  virusScanSendPercentData: 5

  # -- Only if the object is bigger than size then the percentage of data which defined by SendPercentData sent by the
  # c-icap server before receiving the complete body of request.
  virusScanStartSendPercentDataAfter: "2M"

  # -- The maximum size of files which will be scanned by antivirus service.You can use K and M indicators to define
  # size in kilobytes or megabytes.
  virusScanMaxObjectSize: "500M"

  # -- The (kubernetes) service address of clamd tcp socket.
  clamdModClamdHost: "clamav-clamd"

  # -- The clamd tcp socket port.
  clamdModClamdPort: 3310

# -- In seconds, time the given to the pod needs to terminate gracefully.
# Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods
terminationGracePeriodSeconds: ""

# -- Tolerations for pod assignment
# Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
tolerations: []

# -- Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in
# Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/
#
# topologySpreadConstraints:
#   - maxSkew: 1
#     topologyKey: failure-domain.beta.kubernetes.io/zone
#     whenUnsatisfiable: DoNotSchedule
topologySpreadConstraints: []

# Set up update strategy.
#
# Ref: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy
#
# Example:
# updateStrategy:
#  type: RollingUpdate
#  rollingUpdate:
#    maxSurge: 25%
#    maxUnavailable: 25%
updateStrategy:
  # -- Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pod
  # is destroyed first.
  type: "RollingUpdate"
...
