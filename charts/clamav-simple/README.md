<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# clamav-simple

A Helm chart for deploying ClamAV as StatefulSet with RWO Storage (evaluation only)

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository

```console
helm repo add opendesk-clamav https://gitlab.opencode.de/api/v4/projects/1381/packages/helm/stable
helm install my-release --version 4.0.6 opendesk-clamav/clamav-simple
```

### Install via OCI Registry

```console
helm repo add opendesk-clamav oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav
helm install my-release --version 4.0.6 opendesk-clamav/clamav-simple
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set |
| commonAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| commonLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{"drop":["ALL"]}` | Security capabilities for container. |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.runAsGroup | int | `101` | Process group id. |
| containerSecurityContext.runAsNonRoot | bool | `true` | Run container as user. |
| containerSecurityContext.runAsUser | int | `100` | Process user id. |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| extraEnvVars | list | `[]` | Array with extra environment variables to add to containers.  extraEnvVars:   - name: FOO     value: "bar"  |
| extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.registry | string | `"docker.io"` | Container registry address. |
| image.clamav.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.clamav.registry | string | `""` | Container registry address. This setting has higher precedence than global.registry. |
| image.clamav.repository | string | `"clamav/clamav"` | Container repository string. |
| image.clamav.tag | string | `"1.1.1-10_base@sha256:aed8d5a3ef58352c862028fae44241215a50eae0b9acb7ba8892b1edc0a6598f"` | Define image tag. |
| image.icap.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.icap.registry | string | `"registry.souvap-univention.de"` | Container registry address. This setting has higher precedence than global.registry. |
| image.icap.repository | string | `"souvap/tooling/images/c-icap"` | Container repository string. |
| image.icap.tag | string | `"0.5.10@sha256:ca4574d66aa035621336329bf08b8de9f65dc0f6744e3950ca6a0c5ba8cbd8ba"` | Define image tag. |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| initContainer.containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| initContainer.containerSecurityContext.enabled | bool | `true` | Enable security context. |
| initContainer.containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| initContainer.containerSecurityContext.runAsNonRoot | bool | `false` | Run container as user. |
| initContainer.containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| initContainer.enabled | bool | `true` | Enable chown before clamav. |
| lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| livenessProbe.enabled | bool | `true` | Enables kubernetes LivenessProbe. |
| livenessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| livenessProbe.initialDelaySeconds | int | `15` | Delay after container start until LivenessProbe is executed. |
| livenessProbe.periodSeconds | int | `20` | Time between probe executions. |
| livenessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| livenessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| nameOverride | string | `""` | String to partially override release name. |
| nodeSelector | object | `{}` | Node labels for pod assignment Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| persistence.accessModes | list | `["ReadWriteOnce"]` | The volume access modes, some of "ReadWriteOnce", "ReadOnlyMany", "ReadWriteMany", "ReadWriteOncePod".  "ReadWriteOnce" => The volume can be mounted as read-write by a single node. ReadWriteOnce access mode still can                    allow multiple pods to access the volume when the pods are running on the same node. "ReadOnlyMany" => The volume can be mounted as read-only by many nodes. "ReadWriteMany" => The volume can be mounted as read-write by many nodes. "ReadWriteOncePod" => The volume can be mounted as read-write by a single Pod. Use ReadWriteOncePod access mode if                       you want to ensure that only one pod across whole cluster can read that PVC or write to it.  |
| persistence.annotations | object | `{}` | Annotations for the PVC. |
| persistence.dataSource | object | `{}` | Custom PVC data source. |
| persistence.enabled | bool | `true` | Enable data persistence (true) or use temporary storage (false). |
| persistence.existingClaim | string | `""` | Use an already existing claim. |
| persistence.labels | object | `{}` | Labels for the PVC. |
| persistence.selector | object | `{}` | Selector to match an existing Persistent Volume (this value is evaluated as a template)  selector:   matchLabels:     app: my-app  |
| persistence.size | string | `"1Gi"` | The volume size with unit. |
| persistence.storageClass | string | `""` | The (storage) class of PV. |
| persistence.temporaryDirectory.size | string | `"1Gi"` | Size limit of emptyDir volume. |
| podAnnotations | object | `{}` | Pod Annotations. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| podLabels | object | `{}` | Pod Labels. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/ |
| podSecurityContext.enabled | bool | `true` | Enable security context. |
| podSecurityContext.fsGroup | int | `101` | If specified, all processes of the container are also part of the supplementary group |
| podSecurityContext.fsGroupChangePolicy | string | `"Always"` | Change ownership and permission of the volume before being exposed inside a Pod. |
| readinessProbe.enabled | bool | `true` | Enables kubernetes ReadinessProbe. |
| readinessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| readinessProbe.initialDelaySeconds | int | `15` | Delay after container start until ReadinessProbe is executed. |
| readinessProbe.periodSeconds | int | `20` | Time between probe executions. |
| readinessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| readinessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| replicaCount | int | `1` | Set the amount of replicas of deployment. |
| resources.limits.cpu | int | `4` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"4Gi"` | The max amount of RAM to consume. |
| resources.requests.cpu | string | `"10m"` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"2Gi"` | The amount of RAM which has to be available on the scheduled node. |
| service.annotations | object | `{}` | Additional custom annotations |
| service.enabled | bool | `true` | Enable kubernetes service creation. |
| service.ports.clamd.containerPort | int | `3310` | Internal port for ClamAV clamd. |
| service.ports.clamd.port | int | `3310` | Accessible port for ClamAV clamd. |
| service.ports.clamd.protocol | string | `"TCP"` | Milter service protocol. |
| service.ports.icap.containerPort | int | `1344` | Internal port for ClamAV icap. |
| service.ports.icap.port | int | `1344` | Accessible port for ClamAV icap. |
| service.ports.icap.protocol | string | `"TCP"` | Milter service protocol. |
| service.ports.milter.containerPort | int | `7357` | Internal port for ClamAV milter. |
| service.ports.milter.port | int | `7357` | Accessible port for ClamAV milter. |
| service.ports.milter.protocol | string | `"TCP"` | Milter service protocol. |
| service.type | string | `"ClusterIP"` | Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer". |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |
| serviceAccount.automountServiceAccountToken | bool | `false` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |
| serviceAccount.labels | object | `{}` | Additional custom labels for the ServiceAccount. |
| settings.clamd.additionalConfiguration | object | `{}` | Additional custom configuration: |
| settings.clamd.alertBrokenExecutables | string | `"yes"` | With this option clamav will try to detect broken executables (both PE and ELF) and alert on them with the Broken.Executable heuristic signature. |
| settings.clamd.alertBrokenMedia | string | `"no"` | With this option clamav will try to detect broken media file (JPEG, TIFF, PNG, GIF) and alert on them with a Broken.Media heuristic signature. |
| settings.clamd.alertEncrypted | string | `"no"` | Alert on encrypted archives _and_ documents with heuristic signature (encrypted .zip, .7zip, .rar, .pdf). |
| settings.clamd.alertEncryptedArchive | string | `"no"` | Alert on encrypted archives with heuristic signature (encrypted .zip, .7zip, .rar). |
| settings.clamd.alertEncryptedDoc | string | `"no"` | Alert on encrypted archives with heuristic signature (encrypted .pdf) |
| settings.clamd.alertExceedsMax | string | `"no"` | When AlertExceedsMax is set, files exceeding the MaxFileSize, MaxScanSize, or MaxRecursion limit will be flagged with the virus name starting with "Heuristics.Limits.Exceeded". |
| settings.clamd.alertOLE2Macros | string | `"no"` | With this option enabled OLE2 files containing VBA macros, which were not detected by signatures will be marked as "Heuristics.OLE2.ContainsMacros". |
| settings.clamd.alertPartitionIntersection | string | `"no"` | Alert on raw DMG image files containing partition intersections |
| settings.clamd.alertPhishingCloak | string | `"no"` | Alert on cloaked URLs, even if URL isn't in database. This can lead to false positives. |
| settings.clamd.alertPhishingSSLMismatch | string | `"no"` | Alert on SSL mismatches in URLs, even if the URL isn't in the database. This can lead to false positives. |
| settings.clamd.allowAllMatchScan | string | `"yes"` | Permit use of the ALLMATCHSCAN command. If set to no, clamd will reject any ALLMATCHSCAN command as invalid. |
| settings.clamd.bytecode | string | `"yes"` | With this option enabled ClamAV will load bytecode from the database. It is highly recommended you keep this option on, otherwise you'll miss detections for many new viruses. |
| settings.clamd.bytecodeSecurity | string | `"TrustSigned"` | Set bytecode security level. Possible values:   None -      No security at all, meant for debugging.               DO NOT USE THIS ON PRODUCTION SYSTEMS.               This value is only available if clamav was built with --enable-debug!   TrustSigned - Trust bytecode loaded from signed .c[lv]d files, insert runtime safety checks for bytecode loaded                 from other sources.   Paranoid -  Don't trust any bytecode, insert runtime checks for all. Recommended: TrustSigned, because bytecode in .cvd files already has these checks. Note that by default only signed bytecode is loaded, currently you can only load unsigned bytecode in --enable-debug mode. |
| settings.clamd.bytecodeTimeout | int | `10000` | Set bytecode timeout in milliseconds |
| settings.clamd.bytecodeUnsigned | string | `"no"` | Allow loading bytecode from outside digitally signed .c[lv]d files. **Caution**: You should NEVER run bytecode signatures from untrusted sources. Doing so may result in arbitrary code execution. |
| settings.clamd.commandReadTimeout | int | `30` | This option specifies the time (in seconds) after which clamd should timeout if a client doesn't provide any initial command after connecting. |
| settings.clamd.concurrentDatabaseReload | string | `"yes"` | Enable non-blocking (multi-threaded/concurrent) database reloads. This feature will temporarily load a second scanning engine while scanning continues using the first engine. Once loaded, the new engine takes over. The old engine is removed as soon as all scans using the old engine have completed. This feature requires more RAM, so this option is provided in case users are willing to block scans during reload in exchange for lower RAM requirements. |
| settings.clamd.crossFilesystems | string | `"yes"` | Scan files and directories on other filesystems. |
| settings.clamd.debug | string | `"no"` | Enable debug messages in libclamav. |
| settings.clamd.detectPUA | string | `"no"` | # Detect Possibly Unwanted Applications. |
| settings.clamd.disableCache | string | `"no"` | This option allows you to disable the caching feature of the engine. By default, the engine will store an MD5 in a cache of any files that are not flagged as virus or that hit limits checks. Disabling the cache will have a negative performance impact on large scans. |
| settings.clamd.disableCertCheck | string | `"no"` | Certain PE files contain an authenticode signature. By default, we check the signature chain in the PE file against a database of trusted and revoked certificates if the file being scanned is marked as a virus. If any certificate in the chain validates against any trusted root, but does not match any revoked certificate, the file is marked as trusted. If the file does match a revoked certificate, the file is marked as virus. The following setting completely turns off authenticode verification. |
| settings.clamd.exitOnOOM | string | `"yes"` | Stop daemon when libclamav reports out of memory condition. |
| settings.clamd.extendedDetectionInfo | string | `"yes"` | Log additional information about the infected file, such as its size and hash, together with the virus name. |
| settings.clamd.fixStaleSocket | string | `"yes"` | Remove stale socket after unclean shutdown. |
| settings.clamd.followDirectorySymlinks | string | `"no"` | Follow directory symlinks. |
| settings.clamd.followFileSymlinks | string | `"no"` | Follow regular file symlinks. |
| settings.clamd.forceToDisk | string | `"yes"` | This option causes memory or nested map scans to dump the content to disk. If you turn on this option, more data is written to disk and is available when the LeaveTemporaryFiles option is enabled. |
| settings.clamd.generateMetadataJson | string | `"yes"` | Record metadata about the file being scanned. Scan metadata is useful for file analysis purposes and for debugging scan behavior. The JSON metadata will be printed after the scan is complete if Debug is enabled. A metadata.json file will be written to the scan temp directory if LeaveTemporaryFiles is enabled. |
| settings.clamd.heuristicAlerts | string | `"yes"` | In some cases (eg. complex malware, exploits in graphic files, and others), ClamAV uses special algorithms to detect abnormal patterns and behaviors that may be malicious. This option enables alerting on such heuristically detected potential threats. |
| settings.clamd.heuristicScanPrecedence | string | `"no"` | Allow heuristic alerts to take precedence. When enabled, if a heuristic scan (such as phishingScan) detects a possible virus/phish it will stop scan immediately. Recommended, saves CPU scan-time. When disabled, virus/phish detected by heuristic scans will be reported only at the end of a scan. If an archive contains both a heuristically detected virus/phish, and a real malware, the real malware will be reported  Keep this disabled if you intend to handle "Heuristics.*" viruses differently from "real" malware. If a non-heuristically-detected virus (signature-based) is found first, the scan is interrupted immediately, regardless of this config option. |
| settings.clamd.idleTimeout | int | `30` | Waiting for a new job will timeout after this time (seconds). |
| settings.clamd.leaveTemporaryFiles | string | `"no"` | Do not remove temporary files (for debug purposes). |
| settings.clamd.localSocket | string | `"/tmp/clamd.sock"` | Path to a local socket file the daemon will listen on. |
| settings.clamd.localSocketGroup | string | `""` | Sets the group ownership on the unix socket. |
| settings.clamd.localSocketMode | int | `660` | Sets the group ownership on the unix socket. |
| settings.clamd.logClean | string | `"no"` | This option allows to tune what is logged when no threat is found in a scanned message. See logInfected for possible values and caveats. |
| settings.clamd.logFacility | string | `"LOG_LOCAL6"` | Specify the type of syslog messages. |
| settings.clamd.logFile | string | `""` | Path to the log file. |
| settings.clamd.logFileMaxSize | string | `"1M"` | Maximum size of the log file. Value of 0 disables the limit. You may use 'M' or 'm' for megabytes (1M = 1m = 1048576 bytes) and 'K' or 'k' for kilobytes (1K = 1k = 1024 bytes). in bytes just don't use modifiers. If LogFileMaxSize is enabled, log rotation (the LogRotate option) will always be enabled. |
| settings.clamd.logFileUnlock | string | `"no"` | By default the log file is locked for writing - the lock protects against running clamav multiple times. This option disables log file locking. |
| settings.clamd.logInfected | string | `""` | This option allows to tune what is logged when a message is infected. Possible values are Off (the default - nothing is logged), Basic (minimal info logged), Full (verbose info logged)  Note: For this to work properly in sendmail, make sure the msg_id, mail_addr, rcpt_addr and i macroes are available in eom. In other words add a line like: Milter.macros.eom={msg_id}, {mail_addr}, {rcpt_addr}, i to your .cf file. Alternatively use the macro: define(`confMILTER_MACROS_EOM', `{msg_id}, {mail_addr}, {rcpt_addr}, i') Postfix should be working fine with the default settings. |
| settings.clamd.logRotate | string | `"no"` | Enable log rotation. Always enabled when logFileMaxSize is enabled. |
| settings.clamd.logSyslog | string | `"yes"` | Use system logger (can work together with updateLogFile). |
| settings.clamd.logTime | string | `"no"` | Log time with each message. |
| settings.clamd.logVerbose | string | `"no"` | Enable verbose logging |
| settings.clamd.maxConnectionQueueLength | int | `200` | Maximum length the queue of pending connections may grow to. |
| settings.clamd.maxDirectoryRecursion | int | `15` | Maximum depth directories are scanned at. |
| settings.clamd.maxEmbeddedPE | string | `"40M"` | Maximum size of a file to check for embedded PE. Files larger than this value will skip the additional analysis step. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.clamd.maxFileSize | string | `"100M"` | Files larger than this limit won't be scanned. Affects the input file itself as well as files contained inside it (when the input file is an archive, a document or some other kind of container). Value of 0 disables the limit. Note: disabling this limit or setting it too high may result in severe damage to the system. Technical design limitations prevent ClamAV from scanning files greater than 2 GB at this time. |
| settings.clamd.maxFiles | int | `10000` | Number of files to be scanned within an archive, a document, or any other container file. Value of 0 disables the limit. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.clamd.maxHTMLNoTags | string | `"8M"` | Maximum size of a normalized HTML file to scan. HTML files larger than this value after normalization will not be scanned. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.clamd.maxHTMLNormalize | string | `"40M"` | Maximum size of a HTML file to normalize. HTML files larger than this value will not be normalized or scanned. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.clamd.maxIconsPE | int | `100` | This option sets the maximum number of icons within a PE to be scanned. PE files with more icons than this value will have up to the value number icons scanned. Negative values are not allowed. WARNING: setting this limit too high may result in severe damage or impact performance. |
| settings.clamd.maxPartitions | int | `50` | This option sets the maximum number of partitions of a raw disk image to be scanned.    # Raw disk images with more partitions than this value will have up to    # the value number partitions scanned. Negative values are not allowed. |
| settings.clamd.maxQueue | int | `100` | Maximum number of queued items (including those being processed by MaxThreads threads). It is recommended to have this value at least twice MaxThreads if possible. |
| settings.clamd.maxRecHWP3 | int | `16` | This option sets the maximum recursive calls for HWP3 parsing during scanning. HWP3 files using more than this limit will be terminated and alert the user. Scans will be unable to scan any HWP3 attachments if the recursive limit is reached. Negative values are not allowed. WARNING: setting this limit too high may result in severe damage or impact performance. |
| settings.clamd.maxRecursion | int | `17` | Nested archives are scanned recursively, e.g. if a Zip archive contains a RAR file, all files within it will also be scanned. This options specifies how deeply the process should be continued. Note: setting this limit too high may result in severe damage to the system. |
| settings.clamd.maxScanSize | string | `"400M"` | This option sets the maximum amount of data to be scanned for each input file. Archives and other containers are recursively extracted and scanned up to this value. Value of 0 disables the limit Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.clamd.maxScanTime | int | `120000` | This option sets the maximum amount of time to a scan may take. In this version, this field only affects the scan time of ZIP archives. Value of 0 disables the limit. Note: disabling this limit or setting it too high may result allow scanning of certain files to lock up the scanning process/threads resulting in a Denial of Service. Time is in milliseconds. |
| settings.clamd.maxScriptNormalize | string | `"20M"` | Maximum size of a script file to normalize. Script content larger than this value will not be normalized or scanned. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.clamd.maxThreads | int | `10` | Maximum number of threads running at the same time. |
| settings.clamd.maxZipTypeRcg | string | `"1M"` | Maximum size of a ZIP file to reanalyze type recognition. ZIP files larger than this value will skip the step to potentially reanalyze as PE. Note: disabling this limit or setting it too high may result in severe damage to the system. |
| settings.clamd.officialDatabaseOnly | string | `"no"` | Only load the official signatures published by the ClamAV project. |
| settings.clamd.onAccessCurlTimeout | int | `5000` | Max amount of time (in milliseconds) that the OnAccess client should spend for every connect, send, and recieve attempt when communicating with clamd via curl. |
| settings.clamd.onAccessDenyOnError | string | `"no"` | When using prevention, if this option is turned on, any errors that occur during scanning will result in the event attempt being denied. This could potentially lead to unwanted system behaviour with certain configurations, so the client defaults this to off and prefers allowing access events in case of scan or connection error. |
| settings.clamd.onAccessDisableDDD | string | `"no"` | Toggles dynamic directory determination. Allows for recursively watching include paths. |
| settings.clamd.onAccessExcludeRootUID | string | `"no"` | With this option you can exclude the root UID (0). Processes run under root with be able to access all files without triggering scans or permission denied events. Note that if clamd cannot check the uid of the process that generated an on-access scan event (e.g., because OnAccessPrevention was not enabled, and the process already exited), clamd will perform a scan.  Thus, setting OnAccessExcludeRootUID is not *guaranteed* to prevent every access by the root user from triggering a scan (unless OnAccessPrevention is enabled). |
| settings.clamd.onAccessExcludeUID | int | `0` | With this option you can exclude specific UIDs. Processes with these UIDs will be able to access all files without triggering scans or permission denied events. This option can be used multiple times (one per line). Using a value of 0 on any line will disable this option entirely. To exclude the root UID (0) please enable the OnAccessExcludeRootUID option. Also note that if clamd cannot check the uid of the process that generated an on-access scan event (e.g., because OnAccessPrevention was not enabled, and the process already exited), clamd will perform a scan.  Thus, setting OnAccessExcludeUID is not *guaranteed* to prevent every access by the specified uid from triggering a scan (unless OnAccessPrevention is enabled). |
| settings.clamd.onAccessExcludeUname | string | `"no"` | This option allows exclusions via user names when using the on-access scanning client. It can be used multiple times. It has the same potential race condition limitations of the OnAccessExcludeUID option. |
| settings.clamd.onAccessExtraScanning | string | `"no"` | Toggles extra scanning and notifications when a file or directory is created or moved. Requires the DDD system to kick-off extra scans. |
| settings.clamd.onAccessMaxFileSize | string | `"5M"` | Don't scan files larger than OnAccessMaxFileSize Value of 0 disables the limit. |
| settings.clamd.onAccessMaxThreads | int | `5` | Max number of scanning threads to allocate to the OnAccess thread pool at startup. These threads are the ones responsible for creating a connection with the daemon and kicking off scanning after an event has been processed. To prevent clamonacc from consuming all clamd's resources keep this lower than clamd's max threads. |
| settings.clamd.onAccessPrevention | string | `"no"` | Modifies fanotify blocking behaviour when handling permission events. If off, fanotify will only notify if the file scanned is a virus, and not perform any blocking. |
| settings.clamd.onAccessRetryAttempts | int | `0` | Number of times the OnAccess client will retry a failed scan due to connection problems (or other issues). |
| settings.clamd.pcreMatchLimit | int | `100000` | This option sets the maximum calls to the PCRE match function during  an instance of regex matching. Instances using more than this limit will be terminated and alert the user but the scan will continue. For more information on match_limit, see the PCRE documentation. Negative values are not allowed. WARNING: setting this limit too high may severely impact performance. |
| settings.clamd.pcreMaxFileSize | string | `"100M"` | This option sets the maximum filesize for which PCRE subsigs will be executed. Files exceeding this limit will not have PCRE subsigs executed unless a subsig is encompassed to a smaller buffer. Negative values are not allowed. Setting this value to zero disables the limit. WARNING: setting this limit too high or disabling it may severely impact performance. |
| settings.clamd.pcreRecMatchLimit | int | `2000` | This option sets the maximum recursive calls to the PCRE match function during an instance of regex matching. Instances using more than this limit will be terminated and alert the user but the scan will continue. For more information on match_limit_recursion, see the PCRE documentation. Negative values are not allowed and values > PCREMatchLimit are superfluous. WARNING: setting this limit too high may severely impact performance. |
| settings.clamd.phishingScanURLs | string | `"yes"` | With this option enabled ClamAV will try to detect phishing attempts by analyzing URLs found in emails using WDB and PDB signature databases. |
| settings.clamd.phishingSignatures | string | `"yes"` | With this option enabled ClamAV will try to detect phishing attempts by using HTML.Phishing and Email.Phishing NDB signatures. |
| settings.clamd.pidFile | string | `"/tmp/clamd.pid"` | This option allows you to save a process identifier of the listening daemon (main thread). This file will be owned by root, as long as clamd was started by root. It is recommended that the directory where this file is stored is also owned by root to keep other users from tampering with it. |
| settings.clamd.preludeAnalyzerName | string | `"ClamAV"` | Set the name of the analyzer used by prelude-admin. |
| settings.clamd.preludeEnable | string | `"no"` | Enable Prelude output. |
| settings.clamd.readTimeout | int | `120` | Waiting for data from a client socket will timeout after this time (seconds). |
| settings.clamd.scanArchive | string | `"yes"` | ClamAV can scan within archives and compressed files. If you turn off this option, the original files will still be scanned, but without unpacking and additional processing. |
| settings.clamd.scanELF | string | `"yes"` | Executable and Linking Format is a standard format for UN*X executables. This option allows you to control the scanning of ELF files. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.clamd.scanHTML | string | `"yes"` | Perform HTML normalisation and decryption of MS Script Encoder code. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.clamd.scanHWP3 | string | `"yes"` | This option enables scanning of HWP3 files. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.clamd.scanMail | string | `"yes"` | Enable internal e-mail scanner. If you turn off this option, the original files will still be scanned, but without parsing individual messages/attachments. |
| settings.clamd.scanOLE2 | string | `"yes"` | This option enables scanning of OLE2 files, such as Microsoft Office documents and .msi files. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.clamd.scanPDF | string | `"yes"` | This option enables scanning within PDF files. If you turn off this option, the original files will still be scanned, but without decoding and additional processing. |
| settings.clamd.scanPE | string | `"yes"` | PE stands for Portable Executable - it's an executable file format used in all 32 and 64-bit versions of Windows operating systems. This option allows ClamAV to perform a deeper analysis of executable files and it's also required for decompression of popular executable packers such as UPX, FSG, and Petite. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.clamd.scanPartialMessages | string | `"no"` | Scan RFC1341 messages split over many emails. You will need to periodically clean up $TemporaryDirectory/clamav-partial directory. WARNING: This option may open your system to a DoS attack. |
| settings.clamd.scanSWF | string | `"yes"` | This option enables scanning within SWF files. If you turn off this option, the original files will still be scanned, but without decoding and additional processing. |
| settings.clamd.scanXMLDOCS | string | `"yes"` | This option enables scanning xml-based document files supported by libclamav. If you turn off this option, the original files will still be scanned, but without additional processing. |
| settings.clamd.selfCheck | int | `120` |  |
| settings.clamd.sendBufTimeout | int | `500` | This option specifies how long to wait (in milliseconds) if the send buffer is full. Keep this value low to prevent clamd hanging. |
| settings.clamd.streamMaxLength | string | `"100M"` | Close the connection when the data size limit is exceeded. The value should match your MTA's limit for a maximum attachment size. |
| settings.clamd.streamMaxPort | int | `2048` | Limit port range. |
| settings.clamd.streamMinPort | int | `1024` | Limit port range. |
| settings.clamd.structuredCCOnly | string | `"no"` | With this option enabled the DLP module will search for valid Credit Card numbers only. Debit and Private Label cards will not be searched. |
| settings.clamd.structuredDataDetection | string | `"no"` | Enable the DLP module |
| settings.clamd.structuredMinCreditCardCount | int | `3` | This option sets the lowest number of Credit Card numbers found in a file to generate a detect. |
| settings.clamd.structuredMinSSNCount | int | `3` | This option sets the lowest number of Social Security Numbers found in a file to generate a detect. |
| settings.clamd.structuredSSNFormatNormal | string | `"yes"` | With this option enabled the DLP module will search for valid SSNs formatted as xxx-yy-zzzz |
| settings.clamd.structuredSSNFormatStripped | string | `"no"` | With this option enabled the DLP module will search for valid SSNs formatted as xxxyyzzzz |
| settings.clamd.temporaryDirectory | string | `"/var/tmp"` | Optional path to the global temporary directory. |
| settings.clamd.user | string | `"clamav"` | Run as another user (clamd must be started by root for this option to work) |
| settings.clamd.virusEvent | string | `"no"` | Execute a command when virus is found. In the command string %v will be replaced with the virus name and %f will be replaced with the file name. Additionally, two environment variables will be defined: $CLAM_VIRUSEVENT_FILENAME and $CLAM_VIRUSEVENT_VIRUSNAME. |
| settings.freshclam.additionalConfiguration | object | `{}` | Additional custom configuration: |
| settings.freshclam.bytecode | string | `"yes"` | This option enables downloading of bytecode.cvd, which includes additional detection mechanisms and improvements to the ClamAV engine. |
| settings.freshclam.checks | int | `12` | Number of database checks per day. |
| settings.freshclam.compressLocalDatabase | string | `"no"` | By default freshclam will keep the local databases (.cld) uncompressed to make their handling faster. With this option you can enable the compression; the change will take effect with the next database update. |
| settings.freshclam.connectTimeout | string | `"30"` | Timeout in seconds when connecting to database server. |
| settings.freshclam.database.auth | object | `{}` | When using a private ClamAV mirror which requires basic auth. This value will be used for all customURLs and mirror when no specific auth is used.  auth:   username: ""   password: "" |
| settings.freshclam.database.customURLs | list | `[{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/badmacro.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/blurl.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/bofhland_cracked_URL.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/bofhland_malware_attach.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/bofhland_malware_URL.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/bofhland_phishing_URL.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/foxhole_filename.cdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/foxhole_generic.cdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/foxhole_js.cdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/foxhole_js.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/hackingteam.hsb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/junk.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/jurlbl.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/jurlbla.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/lott.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/malwarehash.hsb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/phish.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/phishtank.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/porcupine.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/rogue.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/scam.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/shelter.ldb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/spamattach.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/spamimg.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/spear.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/spearl.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow.attachments.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_bad_cw.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_extended_malware.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_extended_malware_links.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_malware.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_malware_links.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_phish_complete_url.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_spam_complete.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/urlhaus_abuse_ch/downloads/urlhaus.ndb"}]` | Provide custom sources for database files. |
| settings.freshclam.database.mirror.auth | object | `{}` | When using a private ClamAV mirror which requires basic auth. Overrides "settings.database.auth".  auth:   username: ""   password: "" |
| settings.freshclam.database.mirror.enabled | bool | `true` | Enable database mirror usage. |
| settings.freshclam.database.mirror.scheme | string | `"https"` | Database scheme. |
| settings.freshclam.database.mirror.url | string | `"nexus.souvap-univention.de/repository/ClamAV"` | Database URL. |
| settings.freshclam.databaseOwner | string | `"clamav"` | By default when started freshclam drops privileges and switches to the "clamav" user. This directive allows you to change the database owner. |
| settings.freshclam.debug | string | `"no"` | Enable debug messages in libclamav. |
| settings.freshclam.dnsDatabaseInfo | string | `""` | Use DNS to verify virus database version. FreshClam uses DNS TXT records to verify database and software versions. With this directive you can change the database verification domain. WARNING: Do not touch it unless you're configuring freshclam to use your own database verification domain. |
| settings.freshclam.logFacility | string | `"LOG_LOCAL6"` | Specify the type of syslog messages. |
| settings.freshclam.logFileMaxSize | string | `"1M"` | Maximum size of the log file. Value of 0 disables the limit. You may use 'M' or 'm' for megabytes (1M = 1m = 1048576 bytes) and 'K' or 'k' for kilobytes (1K = 1k = 1024 bytes). in bytes just don't use modifiers. If LogFileMaxSize is enabled, log rotation (the LogRotate option) will always be enabled. |
| settings.freshclam.logRotate | string | `"no"` | Enable log rotation. Always enabled when logFileMaxSize is enabled. |
| settings.freshclam.logSyslog | string | `"yes"` | Use system logger (can work together with updateLogFile). |
| settings.freshclam.logTime | string | `"no"` | Log time with each message. |
| settings.freshclam.logVerbose | string | `"no"` | Enable verbose logging |
| settings.freshclam.maxAttempts | int | `3` | How many attempts to make before giving up. |
| settings.freshclam.notifyClamd | string | `"no"` | Send the RELOAD command to clamd. |
| settings.freshclam.onErrorExecute | string | `""` | Run command when database update process fails. |
| settings.freshclam.onOutdatedExecute | string | `""` | Run command when freshclam reports outdated version. In the command string %v will be replaced by the new version number. |
| settings.freshclam.onUpdateExecute | string | `""` | Run command after successful database update. Use EXIT_1 to return 1 after successful database update. |
| settings.freshclam.pidFile | string | `""` | This option allows you to save the process identifier of the daemon This file will be owned by root, as long as freshclam was started by root. It is recommended that the directory where this file is stored is also owned by root to keep other users from tampering with it. |
| settings.freshclam.proxy.enabled | bool | `false` | Enables HTTP Proxy settings. |
| settings.freshclam.proxy.password | string | `"mypass"` | HTTP proxy password. |
| settings.freshclam.proxy.port | string | `"1234"` | HTTP proxy port. |
| settings.freshclam.proxy.server | string | `"https://proxy.example.com"` | The HTTPProxyServer may be prefixed with [scheme]:// to specify which kind of proxy is used.   http://     HTTP Proxy. Default when no scheme or proxy type is specified.   https://    HTTPS Proxy. (Added in 7.52.0 for OpenSSL, GnuTLS and NSS)   socks4://   SOCKS4 Proxy.   socks4a://  SOCKS4a Proxy. Proxy resolves URL hostname.   socks5://   SOCKS5 Proxy.   socks5h://  SOCKS5 Proxy. Proxy resolves URL hostname. |
| settings.freshclam.proxy.username | string | `"myusername"` | HTTP proxy username. |
| settings.freshclam.receiveTimeout | string | `"60"` | Timeout in seconds when reading from database server. 0 means no timeout. |
| settings.freshclam.scriptedUpdates | string | `"yes"` | With this option you can control scripted updates. It's highly recommended to keep it enabled. |
| settings.freshclam.updateLogFile | string | `""` | Path to the log file. |
| settings.icap.additionalConfiguration | object | `{}` | Additional custom configuration: |
| settings.icap.debugLevel | int | `1` | The level of debugging information to be logged. The acceptable range of levels is between 0 and 10. |
| settings.icap.keepAliveTimeout | int | `600` | The maximum time in seconds waiting for a new requests before a connection will be closed. |
| settings.icap.maxKeepAliveRequests | int | `100` | The maximum number of requests can be served by one connection. |
| settings.icap.maxMemObject | int | `131072` | The maximum memory size in bytes taken by an object which is processed by c-icap . If the size of an object's body is larger than the maximum size a temporary file is used. |
| settings.icap.maxRequestsPerChild | int | `0` | The maximum number of requests that a child process can serve. After this number has been reached, the process dies. The goal of this parameter is to minimize the risk of memory leaks and increase the stability of c-icap. It can be disabled by setting its value to 0. |
| settings.icap.maxServers | int | `4` | The maximum allowed number of server processes. |
| settings.icap.maxSpareThreads | int | `20` | If the number of the available threads is more than number, then the c-icap server kills a child. |
| settings.icap.minSpareThreads | int | `10` | If the number of the available threads is less than number, the c-icap server starts a new child. |
| settings.icap.pipelining | string | `"on"` | Enable or disable ICAP requests pipelining |
| settings.icap.remoteProxyUsers | string | `"off"` | Set it to on if you want to use username provided by the proxy server. This is the recommended way to use users in c-icap. If the RemoteProxyUsers is off and c-icap configured to use users or groups the internal authentication mechanism will be used. |
| settings.icap.serverAdmin | string | `"c-icap-admin"` | The Administrator of this server. Used when displaying information about this server (logs, info service, etc). |
| settings.icap.serverName | string | `"c-icap"` | A name for this server. Used when displaying information about this server (logs, info service, etc). |
| settings.icap.startServers | int | `1` | The initial number of server processes. Each server process generates a number of threads, which serve the requests. |
| settings.icap.supportBuggyClients | string | `"off"` | Try to handle requests from buggy clients, for example ICAP requests missing "\r\n" sequences |
| settings.icap.threadsPerChild | int | `10` | The number of threads per child process. |
| settings.icap.timeout | int | `300` | The time in seconds after which a connection without activity can be canceled. |
| settings.icap.tmpDir | string | `"/var/tmp"` | Optional path to the global temporary directory. |
| settings.icap.virusScanMaxObjectSize | string | `"500M"` | The maximum size of files which will be scanned by antivirus service.You can use K and M indicators to define size in kilobytes or megabytes. |
| settings.icap.virusScanScanFileTypes | string | `"TEXT DATA EXECUTABLE ARCHIVE GIF JPEG MSOFFICE"` | The list of file types or groups of file types which will be scanned for viruses. For supported types look in c-icap.magic configuration file. |
| settings.icap.virusScanSendPercentData | int | `5` | The percentage of data that can be sent by the c-icap server before receiving the complete body of a request. This feature in conjuction with the folowing can be usefull becouse if the download of the object takes a lot of time the connection of web client to proxy can be expired. It must be noticed that the data which delivered to the web client maybe contains a virus or a part of a virus and can be dangerous. In the other hand partial data (for example 5% data of a zip or an exe file) in most cases can not be used. Set it to 0 to disable this feature. |
| settings.icap.virusScanStartSendPercentDataAfter | string | `"2M"` | Only if the object is bigger than size then the percentage of data which defined by SendPercentData sent by the c-icap server before receiving the complete body of request. |
| settings.milter.addHeader | string | `"no"` | If this option is set to "Replace" (or "Yes"), an "X-Virus-Scanned" and an "X-Virus-Status" headers will be attached to each processed message, possibly replacing existing headers. If it is set to Add, the X-Virus headers are added possibly on top of the existing ones. Note that while "Replace" can potentially break DKIM signatures, "Add" may confuse procmail and similar filters. |
| settings.milter.additionalConfiguration | object | `{}` | Additional custom configuration: |
| settings.milter.clamdSocket | string | `"unix:/tmp/clamd.sock"` | Define the clamd socket to connect to for scanning. |
| settings.milter.fixStaleSocket | string | `"yes"` | Remove stale socket after unclean shutdown. |
| settings.milter.localNet | list | `[]` | Messages originating from these hosts/networks will not be scanned. This option takes a host(name)/mask pair in CIRD notation and can be repeated several times. If "/mask" is omitted, a host is assumed. To specify a locally originated, non-smtp, email use the keyword "local" |
| settings.milter.logClean | string | `""` | This option allows to tune what is logged when no threat is found in a scanned message. See logInfected for possible values and caveats. |
| settings.milter.logFacility | string | `"LOG_LOCAL6"` | Specify the type of syslog messages. |
| settings.milter.logFile | string | `""` | Path to the log file. |
| settings.milter.logFileMaxSize | string | `"1M"` | Maximum size of the log file. Value of 0 disables the limit. You may use 'M' or 'm' for megabytes (1M = 1m = 1048576 bytes) and 'K' or 'k' for kilobytes (1K = 1k = 1024 bytes). in bytes just don't use modifiers. If LogFileMaxSize is enabled, log rotation (the LogRotate option) will always be enabled. |
| settings.milter.logFileUnlock | string | `"no"` | By default the log file is locked for writing - the lock protects against running clamav-milter multiple times. This option disables log file locking. |
| settings.milter.logInfected | string | `""` | This option allows to tune what is logged when a message is infected. Possible values are Off (the default - nothing is logged), Basic (minimal info logged), Full (verbose info logged)  Note: For this to work properly in sendmail, make sure the msg_id, mail_addr, rcpt_addr and i macroes are available in eom. In other words add a line like: Milter.macros.eom={msg_id}, {mail_addr}, {rcpt_addr}, i to your .cf file. Alternatively use the macro: define(`confMILTER_MACROS_EOM', `{msg_id}, {mail_addr}, {rcpt_addr}, i') Postfix should be working fine with the default settings. |
| settings.milter.logRotate | string | `"no"` | Enable log rotation. Always enabled when logFileMaxSize is enabled. |
| settings.milter.logSyslog | string | `"yes"` | Use system logger (can work together with updateLogFile). |
| settings.milter.logTime | string | `"no"` | Log time with each message. |
| settings.milter.logVerbose | string | `"no"` | Enable verbose logging |
| settings.milter.maxFileSize | string | `"25M"` |  |
| settings.milter.milterSocketGroup | string | `""` | Define the group ownership for the (unix) milter socket. |
| settings.milter.milterSocketMode | string | `""` | Sets the permissions on the (unix) milter socket to the specified mode. |
| settings.milter.onClean | string | `"Accept"` | Action to be performed on clean messages. |
| settings.milter.onFail | string | `"Defer"` | Action to be performed on error conditions (this includes failure to allocate data structures, no scanners available, network timeouts, unknown scanner replies and the like). |
| settings.milter.onInfected | string | `"Quarantine"` | Action to be performed on infected messages |
| settings.milter.pidFile | string | `""` | This option allows you to save a process identifier of the listening daemon (main thread). This file will be owned by root, as long as clamav-milter was started by root. It is recommended that the directory where this file is stored is also owned by root to keep other users from tampering with it. |
| settings.milter.readTimeout | int | `120` | Waiting for data from clamd will timeout after this time (seconds). Value of 0 disables the timeout. |
| settings.milter.rejectMsg | string | `""` | This option allows to set a specific rejection reason for infected messages and it's therefore only useful together with "OnInfected Reject". The string "%v", if present, will be replaced with the virus name. |
| settings.milter.reportHostname | string | `""` |  |
| settings.milter.supportMultipleRecipients | string | `"yes"` | This option affects the behaviour of LogInfected, LogClean and VirusAction when a message with multiple recipients is scanned: If SupportMultipleRecipients is off (the default) then one single log entry is generated for the message and, in case the message is determined to be malicious, the command indicated by VirusAction is executed just once. In both cases only the last recipient is reported. If SupportMultipleRecipients is on: then one line is logged for each recipient and the command indicated by VirusAction is also executed once for each recipient. |
| settings.milter.temporaryDirectory | string | `"/tmp"` | Optional path to the global temporary directory. |
| settings.milter.user | string | `"clamav"` | Run as another user. |
| terminationGracePeriodSeconds | string | `""` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| tolerations | list | `[]` | Tolerations for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| updateStrategy.type | string | `"RollingUpdate"` | Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods is destroyed first. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
