<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# freshclam

A Helm chart for deploying ClamAV freshclam

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository

```console
helm repo add opendesk-clamav https://gitlab.opencode.de/api/v4/projects/1381/packages/helm/stable
helm install my-release --version 4.0.6 opendesk-clamav/freshclam
```

### Install via OCI Registry

```console
helm repo add opendesk-clamav oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav
helm install my-release --version 4.0.6 opendesk-clamav/freshclam
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set |
| commonAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| commonLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{"drop":["ALL"]}` | Security capabilities for container. |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.runAsGroup | int | `101` | Process group id. |
| containerSecurityContext.runAsNonRoot | bool | `true` | Run container as user. |
| containerSecurityContext.runAsUser | int | `100` | Process user id. |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| extraEnvVars | list | `[]` | Array with extra environment variables to add to containers.  extraEnvVars:   - name: FOO     value: "bar"  |
| extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.persistence.claimName | string | `"clamav-db"` | Name of existing shared Persistent Volume Claim. |
| global.registry | string | `"docker.io"` | Container registry address. |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `""` | Container registry address. This setting has higher precedence than global.registry. |
| image.repository | string | `"clamav/clamav"` | Container repository string. |
| image.tag | string | `"1.1.1-10_base@sha256:aed8d5a3ef58352c862028fae44241215a50eae0b9acb7ba8892b1edc0a6598f"` | Define image tag. |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| livenessProbe.enabled | bool | `true` | Enables kubernetes LivenessProbe. |
| livenessProbe.failureThreshold | int | `3` | Number of failed executions until container is terminated. |
| livenessProbe.initialDelaySeconds | int | `10` | Delay after container start until LivenessProbe is executed. |
| livenessProbe.periodSeconds | int | `3` | Time between probe executions. |
| livenessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| livenessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| nameOverride | string | `""` | String to partially override release name. |
| nodeSelector | object | `{}` | Node labels for pod assignment Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| persistence.temporaryDirectory.size | string | `"1Gi"` | Size limit of emptyDir volume. |
| podAnnotations | object | `{}` | Pod Annotations. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| podLabels | object | `{}` | Pod Labels. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/ |
| podSecurityContext.enabled | bool | `true` | Enable security context. |
| podSecurityContext.fsGroup | int | `101` | If specified, all processes of the container are also part of the supplementary group |
| podSecurityContext.fsGroupChangePolicy | string | `"Always"` | Change ownership and permission of the volume before being exposed inside a Pod. |
| readinessProbe.enabled | bool | `true` | Enables kubernetes ReadinessProbe. |
| readinessProbe.failureThreshold | int | `3` | Number of failed executions until container is terminated. |
| readinessProbe.initialDelaySeconds | int | `5` | Delay after container start until ReadinessProbe is executed. |
| readinessProbe.periodSeconds | int | `3` | Time between probe executions. |
| readinessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| readinessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| replicaCount | int | `1` | Set the amount of replicas of deployment. |
| resources.limits.cpu | int | `1` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"1Gi"` | The max amount of RAM to consume. |
| resources.requests.cpu | string | `"10m"` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"16Mi"` | The amount of RAM which has to be available on the scheduled node. |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |
| serviceAccount.automountServiceAccountToken | bool | `false` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |
| serviceAccount.labels | object | `{}` | Additional custom labels for the ServiceAccount. |
| settings.additionalConfiguration | object | `{}` | Additional custom configuration: |
| settings.bytecode | string | `"yes"` | This option enables downloading of bytecode.cvd, which includes additional detection mechanisms and improvements to the ClamAV engine. |
| settings.checks | int | `12` | Number of database checks per day. |
| settings.compressLocalDatabase | string | `"no"` | By default freshclam will keep the local databases (.cld) uncompressed to make their handling faster. With this option you can enable the compression; the change will take effect with the next database update. |
| settings.connectTimeout | string | `"30"` | Timeout in seconds when connecting to database server. |
| settings.database.auth | object | `{}` | When using a private ClamAV mirror which requires basic auth. This value will be used for all customURLs and mirror when no specific auth is used.  auth:   username: ""   password: "" |
| settings.database.customURLs | list | `[{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/badmacro.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/blurl.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/bofhland_cracked_URL.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/bofhland_malware_attach.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/bofhland_malware_URL.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/bofhland_phishing_URL.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/foxhole_filename.cdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/foxhole_generic.cdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/foxhole_js.cdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/foxhole_js.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/hackingteam.hsb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/junk.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/jurlbl.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/jurlbla.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/lott.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/malwarehash.hsb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/phish.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/phishtank.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/porcupine.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/rogue.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/scam.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/shelter.ldb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/spamattach.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/spamimg.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/spear.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/spearl.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow.attachments.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_bad_cw.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_extended_malware.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_extended_malware_links.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_malware.hdb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_malware_links.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_phish_complete_url.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/ftp_swin_edu_au/sanesecurity/winnow_spam_complete.ndb"},{"scheme":"https","url":"nexus.souvap-univention.de/repository/urlhaus_abuse_ch/downloads/urlhaus.ndb"}]` | Provide custom sources for database files. |
| settings.database.mirror.auth | object | `{}` | When using a private ClamAV mirror which requires basic auth. Overrides "settings.database.auth".  auth:   username: ""   password: "" |
| settings.database.mirror.enabled | bool | `true` | Enable database mirror usage. |
| settings.database.mirror.scheme | string | `"https"` | Database scheme. |
| settings.database.mirror.url | string | `"nexus.souvap-univention.de/repository/ClamAV"` | Database URL. |
| settings.databaseOwner | string | `"clamav"` | By default when started freshclam drops privileges and switches to the "clamav" user. This directive allows you to change the database owner. |
| settings.debug | string | `"no"` | Enable debug messages in libclamav. |
| settings.dnsDatabaseInfo | string | `""` | Use DNS to verify virus database version. FreshClam uses DNS TXT records to verify database and software versions. With this directive you can change the database verification domain. WARNING: Do not touch it unless you're configuring freshclam to use your own database verification domain. |
| settings.logFacility | string | `"LOG_LOCAL6"` | Specify the type of syslog messages. |
| settings.logFileMaxSize | string | `"1M"` | Maximum size of the log file. Value of 0 disables the limit. You may use 'M' or 'm' for megabytes (1M = 1m = 1048576 bytes) and 'K' or 'k' for kilobytes (1K = 1k = 1024 bytes). in bytes just don't use modifiers. If LogFileMaxSize is enabled, log rotation (the LogRotate option) will always be enabled. |
| settings.logRotate | string | `"no"` | Enable log rotation. Always enabled when logFileMaxSize is enabled. |
| settings.logSyslog | string | `"yes"` | Use system logger (can work together with updateLogFile). |
| settings.logTime | string | `"no"` | Log time with each message. |
| settings.logVerbose | string | `"no"` | Enable verbose logging |
| settings.maxAttempts | int | `3` | How many attempts to make before giving up. |
| settings.notifyClamd | string | `"no"` | Send the RELOAD command to clamd. |
| settings.onErrorExecute | string | `""` | Run command when database update process fails. |
| settings.onOutdatedExecute | string | `""` | Run command when freshclam reports outdated version. In the command string %v will be replaced by the new version number. |
| settings.onUpdateExecute | string | `""` | Run command after successful database update. Use EXIT_1 to return 1 after successful database update. |
| settings.pidFile | string | `""` | This option allows you to save the process identifier of the daemon This file will be owned by root, as long as freshclam was started by root. It is recommended that the directory where this file is stored is also owned by root to keep other users from tampering with it. |
| settings.proxy.enabled | bool | `false` | Enables HTTP Proxy settings. |
| settings.proxy.password | string | `"mypass"` | HTTP proxy password. |
| settings.proxy.port | string | `"1234"` | HTTP proxy port. |
| settings.proxy.server | string | `"https://proxy.example.com"` | The HTTPProxyServer may be prefixed with [scheme]:// to specify which kind of proxy is used.   http://     HTTP Proxy. Default when no scheme or proxy type is specified.   https://    HTTPS Proxy. (Added in 7.52.0 for OpenSSL, GnuTLS and NSS)   socks4://   SOCKS4 Proxy.   socks4a://  SOCKS4a Proxy. Proxy resolves URL hostname.   socks5://   SOCKS5 Proxy.   socks5h://  SOCKS5 Proxy. Proxy resolves URL hostname. |
| settings.proxy.username | string | `"myusername"` | HTTP proxy username. |
| settings.receiveTimeout | string | `"60"` | Timeout in seconds when reading from database server. 0 means no timeout. |
| settings.scriptedUpdates | string | `"yes"` | With this option you can control scripted updates. It's highly recommended to keep it enabled. |
| settings.updateLogFile | string | `""` | Path to the log file. |
| terminationGracePeriodSeconds | string | `""` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| tolerations | list | `[]` | Tolerations for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| updateStrategy.type | string | `"RollingUpdate"` | Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods is destroyed first. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).
```

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
