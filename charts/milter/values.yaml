# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
#
# SPDX-License-Identifier: Apache-2.0
---
# The global properties are used to configure multiple charts at once.
global:
  # -- Container registry address.
  registry: "docker.io"

  # -- Credentials to fetch images from private registry
  # Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  #
  # imagePullSecrets:
  #   - "docker-registry"
  #
  imagePullSecrets: []

  # This claim is used to share the database with other ClamAV services.
  persistence:
    # -- Name of existing shared Persistent Volume Claim.
    claimName: "clamav-db"

# -- Affinity for pod assignment
# Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
# Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set
affinity: {}

# -- Additional custom annotations to add to all deployed objects.
commonAnnotations: {}

# -- Additional custom labels to add to all deployed objects.
commonLabels: {}

# Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
containerSecurityContext:
  # -- Enable container privileged escalation.
  allowPrivilegeEscalation: false

  # -- Security capabilities for container.
  capabilities:
    drop:
      - "ALL"

  # -- Enable security context.
  enabled: true

  # -- Process user id.
  runAsUser: 100

  # -- Process group id.
  runAsGroup: 101

  # Set Seccomp profile.
  seccompProfile:
    # -- Disallow custom Seccomp profile by setting it to RuntimeDefault.
    type: "RuntimeDefault"

  # -- Mounts the container's root filesystem as read-only.
  readOnlyRootFilesystem: true

  # -- Run container as user.
  runAsNonRoot: true

# -- Array with extra environment variables to add to containers.
#
# extraEnvVars:
#   - name: FOO
#     value: "bar"
#
extraEnvVars: []

# -- Optionally specify extra list of additional volumes
extraVolumes: []

# -- Optionally specify extra list of additional volumeMounts.
extraVolumeMounts: []

# -- Provide a name to substitute for the full names of resources.
fullnameOverride: ""

# Container image section.
image:
  # -- Container registry address. This setting has higher precedence than global.registry.
  registry: ""

  # -- Container repository string.
  repository: "clamav/clamav"

  # -- Define an ImagePullPolicy.
  #
  # Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy
  #
  # "IfNotPresent" => The image is pulled only if it is not already present locally.
  # "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to
  #             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached
  #             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved
  #             digest, and uses that image to launch the container.
  # "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the
  #            kubelet attempts to start the container; otherwise, startup fails
  #
  imagePullPolicy: "IfNotPresent"

  # -- Define image tag.
  tag: "1.1.1-10_base@sha256:aed8d5a3ef58352c862028fae44241215a50eae0b9acb7ba8892b1edc0a6598f"

# -- Credentials to fetch images from private registry
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
#
# imagePullSecrets:
#   - "docker-registry"
#
imagePullSecrets: []

# -- Lifecycle to automate configuration before or after startup
lifecycleHooks: {}

# -- String to partially override release name.
nameOverride: ""

# -- Node labels for pod assignment
# Ref: https://kubernetes.io/docs/user-guide/node-selection/
nodeSelector: {}

#  Configure extra options for containers probes.
livenessProbe:
  # -- Enables kubernetes LivenessProbe.
  enabled: true
  # -- Number of failed executions until container is terminated.
  failureThreshold: 10
  # -- Delay after container start until LivenessProbe is executed.
  initialDelaySeconds: 15
  # -- Time between probe executions.
  periodSeconds: 20
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 5

# Database persistence settings.
persistence:
  # Temporary directory.
  temporaryDirectory:
    # -- Size limit of emptyDir volume.
    size: "1Gi"

# -- Pod Annotations.
# Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/
podAnnotations: {}

# -- Pod Labels.
# Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/
podLabels: {}

# Pod Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
podSecurityContext:
  # -- Enable security context.
  enabled: true

  # -- If specified, all processes of the container are also part of the supplementary group
  fsGroup: 101

  # -- Change ownership and permission of the volume before being exposed inside a Pod.
  fsGroupChangePolicy: "Always"

#  Configure extra options for containers probes.
readinessProbe:
  # -- Enables kubernetes ReadinessProbe.
  enabled: true
  # -- Delay after container start until ReadinessProbe is executed.
  initialDelaySeconds: 15
  # -- Number of failed executions until container is terminated.
  failureThreshold: 10
  # -- Time between probe executions.
  periodSeconds: 20
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 5

# -- Set the amount of replicas of deployment.
replicaCount: 1

# Configure resource requests and limits.
#
# http://kubernetes.io/docs/user-guide/compute-resources/
#
resources:
  limits:
    # -- The max amount of CPUs to consume.
    cpu: 4
    # -- The max amount of RAM to consume.
    memory: "4Gi"
  requests:
    # -- The amount of CPUs which has to be available on the scheduled node.
    cpu: "10m"
    # -- The amount of RAM which has to be available on the scheduled node.
    memory: "2Gi"

# Define and create Kubernetes Service.
#
# Ref.: https://kubernetes.io/docs/concepts/services-networking/service
service:
  # -- Additional custom annotations
  annotations: {}

  # -- Enable kubernetes service creation.
  enabled: true

  # "ClusterIP" => Exposes the Service on a cluster-internal IP. Choosing this value makes the Service only reachable
  #                from within the cluster. This is the default that is used if you don't explicitly specify a type for
  #                a Service.
  # "NodePort" => Exposes the Service on each Node's IP at a static port (the NodePort). To make the node port
  #               available, Kubernetes sets up a cluster IP address, the same as if you had requested a Service of
  #               type: ClusterIP.
  # "LoadBalancer" => Exposes the Service externally using a cloud provider's load balancer.
  #
  # Ref.: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types

  # -- Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer".
  type: "ClusterIP"

  # Define the ports of Service.
  # You can set the port value to an arbitrary value, it will map the container port by name.
  #
  ports:
    milter:
      # -- Accessible port for ClamAV milter.
      port: 7357
      # -- Internal port for ClamAV milter.
      containerPort: 7357
      # -- Milter service protocol.
      protocol: "TCP"

# Service account to use.
# Ref.: https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/
serviceAccount:
  # -- Additional custom annotations for the ServiceAccount.
  annotations: {}

  # -- Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this
  # serviceAccount do not need to use K8s API.
  automountServiceAccountToken: false

  # -- Enable creation of ServiceAccount for pod.
  create: true

  # -- Additional custom labels for the ServiceAccount.
  labels: {}

# ClamAV configuration.
settings:
  # -- Define the group ownership for the (unix) milter socket.
  milterSocketGroup: ""

  # -- Sets the permissions on the (unix) milter socket to the specified mode.
  milterSocketMode: ""

  # -- Remove stale socket after unclean shutdown.
  fixStaleSocket: "yes"

  # -- Waiting for data from clamd will timeout after this time (seconds).
  # Value of 0 disables the timeout.
  readTimeout: 120

  # -- Run as another user.
  user: "clamav"

  # -- This option allows you to save a process identifier of the listening daemon (main thread).
  # This file will be owned by root, as long as clamav-milter was started by root. It is recommended that the directory
  # where this file is stored is also owned by root to keep other users from tampering with it.
  pidFile: ""

  # -- Optional path to the global temporary directory.
  temporaryDirectory: "/tmp"

  # -- Define the clamd host to connect to for scanning.
  clamdHost: "clamd"

  # -- Define the clamd port to connect to for scanning.
  clamdPort: 3310

  # -- Messages originating from these hosts/networks will not be scanned. This option takes a host(name)/mask pair in
  # CIRD notation and can be repeated several times. If "/mask" is omitted, a host is assumed.
  # To specify a locally originated, non-smtp, email use the keyword "local"
  localNet: []

  # Messages larger than this value won't be scanned.
  # Make sure this value is lower or equal than StreamMaxLength in clamd
  maxFileSize: "25M"

  # The following group of options controls the delivery process under
  # different circumstances.
  # The following actions are available:
  # - Accept
  #   The message is accepted for delivery
  # - Reject
  #   Immediately refuse delivery (a 5xx error is returned to the peer)
  # - Defer
  #   Return a temporary failure message (4xx) to the peer
  # - Blackhole (not available for OnFail)
  #   Like Accept but the message is sent to oblivion
  # - Quarantine (not available for OnFail)
  #   Like Accept but message is quarantined instead of being delivered

  # -- Action to be performed on clean messages.
  onClean: "Accept"

  # -- Action to be performed on infected messages
  onInfected: "Quarantine"

  # -- Action to be performed on error conditions (this includes failure to allocate data structures, no scanners
  # available, network timeouts, unknown scanner replies and the like).
  onFail: "Defer"

  # -- This option allows to set a specific rejection reason for infected messages and it's therefore only useful
  # together with "OnInfected Reject". The string "%v", if present, will be replaced with the virus name.
  rejectMsg: ""

  # -- If this option is set to "Replace" (or "Yes"), an "X-Virus-Scanned" and an "X-Virus-Status" headers will be
  # attached to each processed message, possibly replacing existing headers.
  # If it is set to Add, the X-Virus headers are added possibly on top of the existing ones.
  # Note that while "Replace" can potentially break DKIM signatures, "Add" may confuse procmail and similar filters.
  addHeader: "no"

  # When AddHeader is in use, this option allows to arbitrary set the reported hostname. This may be desirable in order
  # to avoid leaking internal names.
  # If unset the real machine name is used.
  reportHostname: ""

  # -- Path to the log file.
  logFile: ""

  # -- By default the log file is locked for writing - the lock protects against running clamav-milter multiple times.
  # This option disables log file locking.
  logFileUnlock: "no"

  # -- Maximum size of the log file.
  # Value of 0 disables the limit.
  # You may use 'M' or 'm' for megabytes (1M = 1m = 1048576 bytes)
  # and 'K' or 'k' for kilobytes (1K = 1k = 1024 bytes).
  # in bytes just don't use modifiers. If LogFileMaxSize is enabled,
  # log rotation (the LogRotate option) will always be enabled.
  logFileMaxSize: "1M"

  # -- Log time with each message.
  logTime: "no"

  # -- Enable verbose logging
  logVerbose: "no"

  # -- Use system logger (can work together with updateLogFile).
  logSyslog: "yes"

  # -- Specify the type of syslog messages.
  logFacility: "LOG_LOCAL6"

  # -- Enable log rotation. Always enabled when logFileMaxSize is enabled.
  logRotate: "no"

  # -- This option allows to tune what is logged when a message is infected.
  # Possible values are Off (the default - nothing is logged), Basic (minimal info logged), Full (verbose info logged)
  #
  # Note:
  # For this to work properly in sendmail, make sure the msg_id, mail_addr, rcpt_addr and i macroes are available in
  # eom. In other words add a line like: Milter.macros.eom={msg_id}, {mail_addr}, {rcpt_addr}, i to your .cf file.
  # Alternatively use the macro:
  # define(`confMILTER_MACROS_EOM', `{msg_id}, {mail_addr}, {rcpt_addr}, i')
  # Postfix should be working fine with the default settings.
  logInfected: ""

  # -- This option allows to tune what is logged when no threat is found in a scanned message.
  # See logInfected for possible values and caveats.
  logClean: ""

  # -- This option affects the behaviour of LogInfected, LogClean and VirusAction when a message with multiple
  # recipients is scanned:
  # If SupportMultipleRecipients is off (the default) then one single log entry is generated for the message and, in
  # case the message is determined to be malicious, the command indicated by VirusAction is executed just once. In both
  # cases only the last recipient is reported.
  # If SupportMultipleRecipients is on: then one line is logged for each recipient and the command indicated by
  # VirusAction is also executed once for each recipient.
  supportMultipleRecipients: "yes"

  # -- Additional custom configuration:
  additionalConfiguration: {}

# -- In seconds, time the given to the pod needs to terminate gracefully.
# Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods
terminationGracePeriodSeconds: ""

# -- Tolerations for pod assignment
# Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
tolerations: []

# -- Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in
# Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/
#
# topologySpreadConstraints:
#   - maxSkew: 1
#     topologyKey: failure-domain.beta.kubernetes.io/zone
#     whenUnsatisfiable: DoNotSchedule
topologySpreadConstraints: []

# Set up update strategy.
#
# Ref: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#strategy
#
# Example:
# updateStrategy:
#  type: RollingUpdate
#  rollingUpdate:
#    maxSurge: 25%
#    maxUnavailable: 25%
updateStrategy:
  # -- Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods
  # is destroyed first.
  type: "RollingUpdate"
...
