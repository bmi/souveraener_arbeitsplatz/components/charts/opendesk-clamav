<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# milter

A Helm chart for deploying ClamAV milter

## Installing the Chart

To install the chart with the release name `my-release`, you have two options:

### Install via Repository

```console
helm repo add opendesk-clamav https://gitlab.opencode.de/api/v4/projects/1381/packages/helm/stable
helm install my-release --version 4.0.6 opendesk-clamav/milter
```

### Install via OCI Registry

```console
helm repo add opendesk-clamav oci://registry.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-clamav
helm install my-release --version 4.0.6 opendesk-clamav/milter
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set |
| commonAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| commonLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{"drop":["ALL"]}` | Security capabilities for container. |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.runAsGroup | int | `101` | Process group id. |
| containerSecurityContext.runAsNonRoot | bool | `true` | Run container as user. |
| containerSecurityContext.runAsUser | int | `100` | Process user id. |
| containerSecurityContext.seccompProfile.type | string | `"RuntimeDefault"` | Disallow custom Seccomp profile by setting it to RuntimeDefault. |
| extraEnvVars | list | `[]` | Array with extra environment variables to add to containers.  extraEnvVars:   - name: FOO     value: "bar"  |
| extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.persistence.claimName | string | `"clamav-db"` | Name of existing shared Persistent Volume Claim. |
| global.registry | string | `"docker.io"` | Container registry address. |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `""` | Container registry address. This setting has higher precedence than global.registry. |
| image.repository | string | `"clamav/clamav"` | Container repository string. |
| image.tag | string | `"1.1.1-10_base@sha256:aed8d5a3ef58352c862028fae44241215a50eae0b9acb7ba8892b1edc0a6598f"` | Define image tag. |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| livenessProbe.enabled | bool | `true` | Enables kubernetes LivenessProbe. |
| livenessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| livenessProbe.initialDelaySeconds | int | `15` | Delay after container start until LivenessProbe is executed. |
| livenessProbe.periodSeconds | int | `20` | Time between probe executions. |
| livenessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| livenessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| nameOverride | string | `""` | String to partially override release name. |
| nodeSelector | object | `{}` | Node labels for pod assignment Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| persistence.temporaryDirectory.size | string | `"1Gi"` | Size limit of emptyDir volume. |
| podAnnotations | object | `{}` | Pod Annotations. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| podLabels | object | `{}` | Pod Labels. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/ |
| podSecurityContext.enabled | bool | `true` | Enable security context. |
| podSecurityContext.fsGroup | int | `101` | If specified, all processes of the container are also part of the supplementary group |
| podSecurityContext.fsGroupChangePolicy | string | `"Always"` | Change ownership and permission of the volume before being exposed inside a Pod. |
| readinessProbe.enabled | bool | `true` | Enables kubernetes ReadinessProbe. |
| readinessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| readinessProbe.initialDelaySeconds | int | `15` | Delay after container start until ReadinessProbe is executed. |
| readinessProbe.periodSeconds | int | `20` | Time between probe executions. |
| readinessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| readinessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| replicaCount | int | `1` | Set the amount of replicas of deployment. |
| resources.limits.cpu | int | `4` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"4Gi"` | The max amount of RAM to consume. |
| resources.requests.cpu | string | `"10m"` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"2Gi"` | The amount of RAM which has to be available on the scheduled node. |
| service.annotations | object | `{}` | Additional custom annotations |
| service.enabled | bool | `true` | Enable kubernetes service creation. |
| service.ports.milter.containerPort | int | `7357` | Internal port for ClamAV milter. |
| service.ports.milter.port | int | `7357` | Accessible port for ClamAV milter. |
| service.ports.milter.protocol | string | `"TCP"` | Milter service protocol. |
| service.type | string | `"ClusterIP"` | Choose the kind of Service, one of "ClusterIP", "NodePort" or "LoadBalancer". |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |
| serviceAccount.automountServiceAccountToken | bool | `false` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |
| serviceAccount.labels | object | `{}` | Additional custom labels for the ServiceAccount. |
| settings.addHeader | string | `"no"` | If this option is set to "Replace" (or "Yes"), an "X-Virus-Scanned" and an "X-Virus-Status" headers will be attached to each processed message, possibly replacing existing headers. If it is set to Add, the X-Virus headers are added possibly on top of the existing ones. Note that while "Replace" can potentially break DKIM signatures, "Add" may confuse procmail and similar filters. |
| settings.additionalConfiguration | object | `{}` | Additional custom configuration: |
| settings.clamdHost | string | `"clamd"` | Define the clamd host to connect to for scanning. |
| settings.clamdPort | int | `3310` | Define the clamd port to connect to for scanning. |
| settings.fixStaleSocket | string | `"yes"` | Remove stale socket after unclean shutdown. |
| settings.localNet | list | `[]` | Messages originating from these hosts/networks will not be scanned. This option takes a host(name)/mask pair in CIRD notation and can be repeated several times. If "/mask" is omitted, a host is assumed. To specify a locally originated, non-smtp, email use the keyword "local" |
| settings.logClean | string | `""` | This option allows to tune what is logged when no threat is found in a scanned message. See logInfected for possible values and caveats. |
| settings.logFacility | string | `"LOG_LOCAL6"` | Specify the type of syslog messages. |
| settings.logFile | string | `""` | Path to the log file. |
| settings.logFileMaxSize | string | `"1M"` | Maximum size of the log file. Value of 0 disables the limit. You may use 'M' or 'm' for megabytes (1M = 1m = 1048576 bytes) and 'K' or 'k' for kilobytes (1K = 1k = 1024 bytes). in bytes just don't use modifiers. If LogFileMaxSize is enabled, log rotation (the LogRotate option) will always be enabled. |
| settings.logFileUnlock | string | `"no"` | By default the log file is locked for writing - the lock protects against running clamav-milter multiple times. This option disables log file locking. |
| settings.logInfected | string | `""` | This option allows to tune what is logged when a message is infected. Possible values are Off (the default - nothing is logged), Basic (minimal info logged), Full (verbose info logged)  Note: For this to work properly in sendmail, make sure the msg_id, mail_addr, rcpt_addr and i macroes are available in eom. In other words add a line like: Milter.macros.eom={msg_id}, {mail_addr}, {rcpt_addr}, i to your .cf file. Alternatively use the macro: define(`confMILTER_MACROS_EOM', `{msg_id}, {mail_addr}, {rcpt_addr}, i') Postfix should be working fine with the default settings. |
| settings.logRotate | string | `"no"` | Enable log rotation. Always enabled when logFileMaxSize is enabled. |
| settings.logSyslog | string | `"yes"` | Use system logger (can work together with updateLogFile). |
| settings.logTime | string | `"no"` | Log time with each message. |
| settings.logVerbose | string | `"no"` | Enable verbose logging |
| settings.maxFileSize | string | `"25M"` |  |
| settings.milterSocketGroup | string | `""` | Define the group ownership for the (unix) milter socket. |
| settings.milterSocketMode | string | `""` | Sets the permissions on the (unix) milter socket to the specified mode. |
| settings.onClean | string | `"Accept"` | Action to be performed on clean messages. |
| settings.onFail | string | `"Defer"` | Action to be performed on error conditions (this includes failure to allocate data structures, no scanners available, network timeouts, unknown scanner replies and the like). |
| settings.onInfected | string | `"Quarantine"` | Action to be performed on infected messages |
| settings.pidFile | string | `""` | This option allows you to save a process identifier of the listening daemon (main thread). This file will be owned by root, as long as clamav-milter was started by root. It is recommended that the directory where this file is stored is also owned by root to keep other users from tampering with it. |
| settings.readTimeout | int | `120` | Waiting for data from clamd will timeout after this time (seconds). Value of 0 disables the timeout. |
| settings.rejectMsg | string | `""` | This option allows to set a specific rejection reason for infected messages and it's therefore only useful together with "OnInfected Reject". The string "%v", if present, will be replaced with the virus name. |
| settings.reportHostname | string | `""` |  |
| settings.supportMultipleRecipients | string | `"yes"` | This option affects the behaviour of LogInfected, LogClean and VirusAction when a message with multiple recipients is scanned: If SupportMultipleRecipients is off (the default) then one single log entry is generated for the message and, in case the message is determined to be malicious, the command indicated by VirusAction is executed just once. In both cases only the last recipient is reported. If SupportMultipleRecipients is on: then one line is logged for each recipient and the command indicated by VirusAction is also executed once for each recipient. |
| settings.temporaryDirectory | string | `"/tmp"` | Optional path to the global temporary directory. |
| settings.user | string | `"clamav"` | Run as another user. |
| terminationGracePeriodSeconds | string | `""` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| tolerations | list | `[]` | Tolerations for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| updateStrategy.type | string | `"RollingUpdate"` | Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods is destroyed first. |

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
